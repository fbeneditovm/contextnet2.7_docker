FROM adoptopenjdk:8-jre-hotspot-focal

WORKDIR /contextnet
COPY ./contextnet-2.7.jar .
COPY HDE /opt/OpenSplice/HDE

ENV OSPL_HOME=/opt/OpenSplice/HDE/x86_64.linux
ENV PATH=$OSPL_HOME/bin:$PATH
ENV LD_LIBRARY_PATH=$OSPL_HOME/lib:$LD_LIBRARY_PATH
ENV CPATH=$OSPL_HOME/include:$OSPL_HOME/include/sys:$CPATH
ENV OSPL_URI=file://$OSPL_HOME/etc/config/ospl.xml

RUN apt update
RUN apt install -y build-essential
ENV TZ=America/Fortaleza
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

CMD ["java", "-jar", "contextnet-2.7.jar", "127.0.0.1", "5500", "OpenSplice"]