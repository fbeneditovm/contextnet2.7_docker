# ContextNet 2.7 container (from adoptopenjdk:8-jre-hotspot-focal, uses OpenSplice 6.7)
A container with a ContextNet 2.7 gateway.

Based on an AdopOpenJDK container:

https://hub.docker.com/_/adoptopenjdk

Uses OpenSplice 6.7:

https://github.com/ADLINK-IST/opensplice

## Dependency:

You need to have docker installed.

## Usage:

Open the repository folder on the terminal after you clone it.

1. Build the image
```bash
docker build -t contextnet:2.7-adopt-os6.9 .
```
2. Run a new container
```bash
docker run -d -p 5500:5500/udp --name contextnet-gw contextnet:2.7-adopt-os6.9
```
**Note:** This commands will attach the service to the port 5500. To choose another port you need to ajust the docker run command and the last line of the Dockerfile.